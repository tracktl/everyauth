var oauthModule = require('./oauth2')
  , url = require('url');

var fb = module.exports =
oauthModule.submodule('deezer')
  .configurable({
      scope: 'specify types of access: See http://developers.deezer.com/api/permissions'
  })

  .apiHost('http://api.deezer.com')
  .oauthHost('https://connect.deezer.com')

  .authPath('/oauth/auth.php')
  .accessTokenPath('/oauth/access_token.php')

  .entryPath('/auth/deezer')
  .callbackPath('/auth/deezer/callback')

  .authQueryParam('perms', function () {
    return this._scope && this.scope();
  })
  .authQueryParam('app_id', function () {
    return this._appId && this.appId();
  })
  .authQueryParam('response_type', function () {
    return 'code';
  })

  .accessTokenParam('secret', function () {
    return this._appSecret && this.appSecret();
  })
  .accessTokenParam('app_id', function () {
    return this._appId && this.appId();
  })
  .accessTokenParam('grant_type', function () {
    return 'authorization_code';
  })

  .authCallbackDidErr( function (req) {
    var parsedUrl = url.parse(req.url, true);
    return parsedUrl.query && !!parsedUrl.query.error;
  })
  .handleAuthCallbackError( function (req, res) {
    var parsedUrl = url.parse(req.url, true)
      , errorDesc = parsedUrl.query.error_description;
    if (res.render) {
      res.render(__dirname + '/../views/auth-fail.jade', {
        errorDescription: errorDesc
      });
    } else {
      // TODO Replace this with a nice fallback
      throw new Error("You must configure handleAuthCallbackError if you are not using express");
    }
  })

  .fetchOAuthUser( function (accessToken) {
    var p = this.Promise();
    this.oauth.get(this.apiHost() + '/user/me', accessToken, function (err, data) {
      if (err) return p.fail(err);
      var oauthUser = JSON.parse(data);
      p.fulfill(oauthUser);
    })
    return p;
  })
  .moduleErrback( function (err, seqValues) {
    if (err instanceof Error) {
      var next = seqValues.next;
      return next(err);
    } else if (err.extra) {
      var facebookResponse = err.extra.res
        , serverResponse = seqValues.res;
      serverResponse.writeHead(
          facebookResponse.statusCode
        , facebookResponse.headers);
      serverResponse.end(err.extra.data);
    } else if (err.statusCode) {
      var serverResponse = seqValues.res;
      serverResponse.writeHead(err.statusCode);
      serverResponse.end(err.data);
    } else {
      console.error(err);
      throw new Error('Unsupported error type');
    }
  });
