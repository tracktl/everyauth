0.3.0

- No longer need to invoke `everyauth.helpExpress`
- Lack of `everyauth.everymodule.findUserById` now throws an Error
- Add in an `everyauth.everymodule.userPkey` configurable for data schemas that
  do not use `id` as the identifying property of a user.
- Use https://github.com/mikeal/request instead of restler.
- Improved error handling.
- facebook.popup shortcut.
- facebook now has a `fields` configurable to specify what user fields to fetch
  from Facebook when retrieving the facebook user data.
- Provide optional request object to findUserById callback.
- New module method `configure` for better coffee-script syntax.
- New modules - soundcloud, mailchimp, mail.ru
